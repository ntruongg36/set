/** 
 * Copyright (c) 2023 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.feature.table.pt1;

<<<<<<< HEAD:java/bundles/org.eclipse.set.feature.table/src/org/eclipse/set/feature/table/PlanPro2TableTransformationService.java
import java.util.Set;

=======
import org.eclipse.set.model.tablemodel.ColumnDescriptor;
import org.eclipse.set.model.tablemodel.Table;
import org.eclipse.set.model.tablemodel.extensions.ColumnDescriptorExtensions;
>>>>>>> 060bc5de (Refactor table implementation):java/bundles/org.eclipse.set.feature.table.pt1/src/org/eclipse/set/feature/table/pt1/AbstractPlanPro2TableTransformationService.java
import org.eclipse.set.ppmodel.extensions.container.MultiContainer_AttributeGroup;
import org.eclipse.set.ppmodel.extensions.utils.TableNameInfo;
import org.eclipse.set.utils.table.AbstractTableTransformationService;
import org.eclipse.set.utils.table.ColumnDescriptorModelBuilder;

/**
 * Common base for tables in this bundle
<<<<<<< HEAD:java/bundles/org.eclipse.set.feature.table/src/org/eclipse/set/feature/table/PlanPro2TableTransformationService.java
 */
public abstract class PlanPro2TableTransformationService extends
		AbstractTableTransformationService<MultiContainer_AttributeGroup> {
=======
 * 
 * @param <C>
 *            Columns type
 */
public abstract class AbstractPlanPro2TableTransformationService<C extends AbstractTableColumns>
		extends
		AbstractTableTransformationService<MultiContainer_AttributeGroup> {
	@Override
	protected ColumnDescriptor buildHeading(final Table table) {
		// Add missing heading units
		final ColumnDescriptor root = super.buildHeading(table);
		ColumnDescriptorExtensions.addMissingHeadingUnits(root);
		return root;
	}
>>>>>>> 060bc5de (Refactor table implementation):java/bundles/org.eclipse.set.feature.table.pt1/src/org/eclipse/set/feature/table/pt1/AbstractPlanPro2TableTransformationService.java

	protected C columns;

	@Override
	public ColumnDescriptor fillHeaderDescriptions(
			final ColumnDescriptorModelBuilder builder) {
		return columns.fillHeaderDescriptions(builder);
	}

	/**
	 * @return the table name info
	 */
	public abstract TableNameInfo getTableNameInfo();

	/**
	 * @return position of fixed columns
	 */
	public abstract Set<Integer> getFixedColumnsPos();
}
